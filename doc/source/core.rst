.. NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
..
.. NLDSL is licensed under a
.. Creative Commons Attribution-NonCommercial 3.0 Unported License.
..
.. You should have received a copy of the license along with this
.. work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

Core Module
***********

.. automodule:: nldsl.core


Code Generation
===============

.. automodule:: nldsl.core.codegen


.. autoclass:: nldsl.core.CodeMap
   :show-inheritance:
   :members:

   .. automethod:: __getitem__
   .. automethod:: __setitem__
   .. automethod:: __delitem__
   .. automethod:: __iter__
   .. automethod:: __len__


.. autoclass:: nldsl.core.CodeGenerator
   :show-inheritance:
   :members:

   .. automethod:: __call__


.. autoclass:: nldsl.core.Recommendation
	:members:


External Dynamic Grammar
========================

.. automodule:: nldsl.core.external


.. autodecorator:: nldsl.core.grammar	


.. autoclass:: nldsl.core.GrammarFunction
   :members:

   .. automethod:: __call__


.. autoclass:: nldsl.core.GrammarParser
   :members:

   .. automethod:: __call__


Internal Dynamic Grammar
========================

.. automodule:: nldsl.core.internal


.. autoclass:: nldsl.core.PipeFunction
   :members:

   .. automethod:: __call__


.. autoclass:: nldsl.core.IdentityConverter
   :members:

   .. automethod:: __call__


.. autoclass:: nldsl.core.NameConverter
   :members:

   .. automethod:: __call__


.. autoclass:: nldsl.core.ListConverter
   :members:

   .. automethod:: __call__


Dynamic Grammar Rules
=====================

.. automodule:: nldsl.core.rules


.. autoclass:: nldsl.core.GrammarRule
   :members:

   .. automethod:: __call__


.. autoclass:: nldsl.core.KeywordRule
   :members:

   .. automethod:: __call__


.. autoclass:: nldsl.core.VariableRule
   :members:

   .. automethod:: __call__


.. autoclass:: nldsl.core.VarListRule
   :members:

   .. automethod:: __call__


.. autoclass:: nldsl.core.ExpressionRule
	:members:
	:special-members: __call__



Argument and Operator Types
===========================

.. automodule:: nldsl.core.types


.. autoclass:: nldsl.core.OperatorType
	:show-inheritance:
	:members:


.. autoclass:: nldsl.core.Type
	:members:


.. autoclass:: nldsl.core.Value
	:show-inheritance:
	:members:


.. autoclass:: nldsl.core.Keyword
	:show-inheritance:
	:members:


.. autoclass:: nldsl.core.Variable
	:show-inheritance:
	:members:


.. autoclass:: nldsl.core.Expression
	:show-inheritance:
	:members:


.. autoclass:: nldsl.core.VarList
	:show-inheritance:
	:members:


.. autofunction:: nldsl.core.is_type


.. autofunction:: nldsl.core.is_value


.. autofunction:: nldsl.core.is_keyword


.. autofunction:: nldsl.core.is_variable


.. autofunction:: nldsl.core.is_expression


.. autofunction:: nldsl.core.is_var_list


Utility functions
=================

.. automodule:: nldsl.core.utils


.. autodecorator:: nldsl.core.decorator_factory


.. autofunction:: nldsl.core.cast_down


.. autofunction:: nldsl.core.convert_function


.. autofunction:: nldsl.core.split_code


.. autofunction:: nldsl.core.list_to_string


Exceptions
==========

.. automodule:: nldsl.core.exceptions


.. autoclass:: nldsl.core.DSLError


.. autoclass:: nldsl.core.DSLArgumentError


.. autoclass:: nldsl.core.DSLFunctionError