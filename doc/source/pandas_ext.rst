.. NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
..
.. NLDSL is licensed under a
.. Creative Commons Attribution-NonCommercial 3.0 Unported License.
..
.. You should have received a copy of the license along with this
.. work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

Pandas Extension
****************

.. automodule:: nldsl.pandas_extension


Grammar Rules
=============

.. autofunction:: nldsl.pandas_extension.expression_only

.. autofunction:: nldsl.pandas_extension.on_dataframe

.. autofunction:: nldsl.pandas_extension.create_dataframe

.. autofunction:: nldsl.pandas_extension.load_from

.. autofunction:: nldsl.pandas_extension.save_to

.. autofunction:: nldsl.pandas_extension.union

.. autofunction:: nldsl.pandas_extension.difference

.. autofunction:: nldsl.pandas_extension.intersection

.. autofunction:: nldsl.pandas_extension.select_columns

.. autofunction:: nldsl.pandas_extension.select_rows

.. autofunction:: nldsl.pandas_extension.drop_columns

.. autofunction:: nldsl.pandas_extension.join

.. autofunction:: nldsl.pandas_extension.group_by

.. autofunction:: nldsl.pandas_extension.replace_values

.. autofunction:: nldsl.pandas_extension.append_column

.. autofunction:: nldsl.pandas_extension.sort_by

.. autofunction:: nldsl.pandas_extension.drop_duplicates

.. autofunction:: nldsl.pandas_extension.rename_columns

.. autofunction:: nldsl.pandas_extension.show

.. autofunction:: nldsl.pandas_extension.show_schema

.. autofunction:: nldsl.pandas_extension.describe

.. autofunction:: nldsl.pandas_extension.head

.. autofunction:: nldsl.pandas_extension.count


Code Generator
==============

.. autoclass:: nldsl.pandas_extension.PandasCodeGenerator
   :show-inheritance:
   :members:


.. autoclass:: nldsl.pandas_extension.PandasExpressionRule
   :show-inheritance:
   :members:
