# NLDSL (c) 2019 by Kevin Kiefer <abc.kiefer@gmail.com>, Heidelberg University
#
# NLDSL is licensed under a
# Creative Commons Attribution-NonCommercial 3.0 Unported License.
#
# You should have received a copy of the license along with this
# work.  If not, see <http://creativecommons.org/licenses/by-nc/3.0/>.

import unittest
from inspect import signature
from nldsl.core.utils import decorator_factory, cast_down, convert_function, split_code




class TestDecoratorFactory(unittest.TestCase):
    def test_decorator_factory(self):
        @decorator_factory
        def my_decorator(fun):
            return fun

        @my_decorator
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 12)


    def test_decorator_factory_with_args(self):
        @decorator_factory
        def my_decorator(fun, a, b):
            return lambda x, y: fun(x, y) + a + b

        @my_decorator(2, 3)
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 17)


    def test_decorator_factory_with_default_args(self):
        @decorator_factory
        def my_decorator(fun, a=10, b=10):
            return lambda x, y: fun(x, y) + a + b

        @my_decorator(5)
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 27)


    def test_decorator_factory_with_kwargs(self):
        @decorator_factory
        def my_decorator(fun, a=10, b=10):
            return lambda x, y: fun(x, y) + a + b

        @my_decorator(a=2, b=3)
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 17)


    def test_decorator_factory_with_default_kwargs(self):
        @decorator_factory
        def my_decorator(fun, a=10, b=10):
            return lambda x, y: fun(x, y) + a + b

        @my_decorator(b=5)
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 27)


    def test_decorator_factory_with_generic_args(self):
        @decorator_factory
        def my_decorator(fun, *args):
            return lambda x, y: fun(x, y) + sum(args)

        @my_decorator(2, 3, 5, 7, 13)
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 42)


    def test_decorator_factory_with_generic_kwargs(self):
        @decorator_factory
        def my_decorator(fun, **kwargs):
            return lambda x, y: fun(x, y) + sum(kwargs.values())

        @my_decorator(a=2, c=3, b=5, d=7, e=13)
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 42)


    def test_decorator_factory_with_args_and_kwargs(self):
        @decorator_factory
        def my_decorator(fun, a, b, c, d, e):
            return lambda x, y: fun(x, y) + a + b + c + d + e

        @my_decorator(2, 3, c=5, d=7, e=13)
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 42)


    def test_decorator_factory_with_default_args_and_kwargs(self):
        @decorator_factory
        def my_decorator(fun, a=10, b=11, c=12, d=13, e=14):
            return lambda x, y: fun(x, y) + a + b + c + d + e

        @my_decorator(2, c=5, e=7)
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 50)


    def test_decorator_factory_with_generic__args_and_kwargs(self):
        @decorator_factory
        def my_decorator(fun, *args, **kwargs):
            return lambda x, y: fun(x, y) + sum(args) + sum(kwargs.values())

        @my_decorator(2, 3, c=5, e=7, a=13)
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 42)

    def test_decorator_factory_with_generic__args_and_kwargs(self):
        def my_decorator(a=10, b=10):
            def _decorator(fun):
                return lambda x, y: fun(x, y) + a + b
            return _decorator

        @my_decorator()
        def my_fun(x, y):
            return x + y

        self.assertEqual(my_fun(5, 7), 32)




class TestCastDown(object):
    def test_cast_int(self):
        self.assertIsInstance(cast_down(7), int)
        self.assertEqual(cast_down(7), 7)

    def test_cast_float(self):
        self.assertIsInstance(cast_down(7.0), int)
        self.assertEqual(cast_down(7.0), 7)


    def test_no_cast_float(self):
        self.assertIsInstance(cast_down(7.7), float)
        self.assertEqual(cast_down(7.7), 7.7)


    def test_cast_string(self):
        self.assertIsInstance(cast_down("7"), str)
        self.assertEqual(cast_down("7"), "7")




class TestConvertFunction(unittest.TestCase):
    def test_convert_fun_code(self):
        def fun(code):
            return code
        res = convert_function(fun)
        self.assertEqual(str(signature(res)), "(code, args, env)")

    def test_convert_fun_args(self):
        def fun(code, args):
            return code + args
        res = convert_function(fun)
        self.assertEqual(str(signature(res)), "(code, args, env)")

    def test_convert_fun_env(self):
        def fun(code, env):
            return code + env
        res = convert_function(fun)
        self.assertEqual(str(signature(res)), "(code, args, env)")

    def test_convert_fun(self):
        def fun(code, args, env):
            return code + args + env
        res = convert_function(fun)
        self.assertEqual(str(signature(res)), "(code, args, env)")

    def test_convert_fun_should_raise(self):
        def fun(code, foo):
            return code + foo
        self.assertRaises(ValueError, convert_function, fun)

    def test_convert_fun_should_not_raise(self):
        def fun(ccc, foo, bar):
            return ccc, foo, bar
        res = convert_function(fun)
        self.assertEqual(str(signature(res)), "(ccc, foo, bar)")




class TestSplitCode(unittest.TestCase):
    def test_split_with_assign(self):
        code = "x = on df : select rows df.col1 < 7 : union df2"
        lhs, rhs = split_code(code)

        self.assertEqual(lhs, "x")
        self.assertEqual(rhs, "on df : select rows df.col1 < 7 : union df2")


    def test_split_no_assign(self):
        code = "on df : select rows df.col1 < 7 : union df2"
        lhs, rhs = split_code(code)

        self.assertEqual(lhs, "")
        self.assertEqual(rhs, "on df : select rows df.col1 < 7 : union df2")




if __name__ == "__main__":
    unittest.main(verbosity=2)
